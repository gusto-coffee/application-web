import { Box, ColorModeProvider, CSSReset, ThemeProvider, useColorMode } from '@chakra-ui/core';
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import EditProfile from './Screens/EditProfile';
import Footer from './Components/Footer';
import Header from './Components/Header';
import ScrollToTop from './Components/scrollToTop';
import { AuthProvider } from './Hooks/useAuth';
import Apropos from './Screens/Apropos';
import Cart from './Screens/Cart';
import Cgu from './Screens/Cgucgv';
import ContactUs from './Screens/Contact';
import Cookies from './Screens/Cookies';
import Coworking from './Screens/Coworking';
import CoworkingDetail from './Screens/CoworkingDetail';
import ForgotPassword from './Screens/ForgotPassword';
import Home from './Screens/Home';
import Login from './Screens/Login';
import Mention from './Screens/Mentions';
import Menu from './Screens/Menu';
import Payment from './Screens/Payment';
import Profile from './Screens/Profile';
import ReinitializationPassword from './Screens/ReinitalizationPassword';
import Services from './Screens/Services';
import Signup from './Screens/Signup';
import customTheme from './theme';
import UserOrders from './Screens/UserOrders';
import UserOrder from './Screens/UserOrder';

const App = ({ children }) => {
  const { colorMode } = useColorMode();

  return (
    <Box className='app' backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <AuthProvider>
        <ThemeProvider theme={customTheme}>
          <ColorModeProvider>
            <CSSReset />
            {children}
            <Router>
              <Header />
              <ScrollToTop />
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/coworking' component={Coworking} />
                <Route exact path='/coworking/:name' component={CoworkingDetail} />
                <Route exact path='/la-carte' component={Menu} />
                <Route exact path='/services' component={Services} />
                <Route exact path='/panier' component={Cart} />
                <Route exact path='/panier/:id/paiement' component={Payment} />
                <Route exact path='/signup' component={Signup} />
                <Route exact path='/login' component={Login} />
                <Route exact path='/profil' component={Profile} />
                <Route exact path='/profil/modifier-mon-profil' component={EditProfile} />
                <Route exact path='/profil/mes-commandes' component={UserOrders} />
                <Route exact path='/profil/mes-commandes/:id' component={UserOrder} />
                <Route exact path='/mentions-legales' component={Mention} />
                <Route exact path='/cookies' component={Cookies} />
                <Route exact path='/cgu-cgv' component={Cgu} />
                <Route exact path='/a-propos' component={Apropos} />
                <Route exact path='/contact' component={ContactUs} />
                <Route exact path='/mot-de-passe-oublie' component={ForgotPassword} />
                <Route exact path='/reinitialisation-de-mon-mot-de-passe/:id' component={ReinitializationPassword} />
              </Switch>
              <Footer />
            </Router>
          </ColorModeProvider>
        </ThemeProvider>
      </AuthProvider>
    </Box>
  );
};

export default App;
