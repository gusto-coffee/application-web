import io from 'socket.io-client';
import { config } from './config';

const __URL = config.BASE_API_URL;

// options de la connexion
const __OPTIONS = {
  query: { token: JSON.parse(localStorage.getItem('token')) || null, room: 'NWS2020' },
  secure: false,
};

export const socket = io(__URL, __OPTIONS);
