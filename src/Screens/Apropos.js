import {Box, Flex, Heading, Image, Text, useColorMode} from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';

const Apropos = () => {
  const { colorMode } = useColorMode();
  return (
      <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'} py='80px' px='50px'>
        <MetaTags>
          <title>À propos de Gusto Coffee</title>
          <meta name="description" content="Gusto Coffee : Une entreprise fort de café ! Découvrez la génèse de notre entreprise et son cheminement vers le coworking" />
          <meta charSet="UTF-8"/>
        </MetaTags>
        <Heading as='h2' textAlign='center' my={10}>
          À propos de Gusto-Coffee
        </Heading>
        <Flex>
          <Box maxW='900px' p='20px' ml='25%' mr='25%'>
            <Image
                rounded='md'
                src='https://images7.alphacoders.com/394/thumb-1920-394204.jpg'
                alt='At Gusto-Cofee, we love Coffee since we are babies !'
            />
          </Box>
        </Flex>
        <Text mx={100} my={10} mr={200}>
          La société est née d’un véritable Amour pour le café. M. Landemaine et M. Bellucci, associés depuis 1999 et spécialisés dans la vente de
          produits divers autours du café, ont donné naissance à Gusto-Coffee.
        </Text>
        <Text mx={100} my={10} mr={200}>
          Fort de son savoir-faire en matière de café, Gusto-Coffee ouvre son 1er coffee-shop en Angleterre en 2000 et en Italie, pays reconnu pour sa
          tradition en matière de café, en 2002. Dès ces débuts, notre société a choisit d’être une entreprise différente, d'avoir une identité, un coup
          de patte, qui non seulement célébrait le café, son goût si proche de la perfection et sa riche tradition mais remettre l'humain au coeur de nos
          vies en favorisant les rapports humains.
        </Text>
        <Text mx={100} my={10} mr={200}>
          C'est lors d’un voyage personnel en Irlande en 1999, M. Landemaine se voit dans l'obligation de travailler dans un espace coworking Irlandais,
          une sombre histoire de bug de l'an 2000... Après de longues prises de tête avec son service informatique, il avait besoin d'un café ! Il se
          dirige donc vers la machine de la salle. Erreur Fatale ! Le café est un véritable jus de chaussettes. Íl se pose donc la question suivante :
        </Text>
        <Text mx={100} my={10} mr={200} textAlign='center'>
          <h3 style={{ fontWeight: 'bold' }}>
            Pourquoi les gens qui travaillent n'aurait pas le droit à un café décent ? Pourquoi doivent-ils boire cette infamie qu'ils osent appeller café
            ?
          </h3>
        </Text>
        <Text mx={100} my={10} mr={200}>
          C’est à ce moment-là que l'idée d’innover et de marier le concept de café de haute qualité et lieu de travail collaboratif s’est précisée. Dès
          son retour, il expose son projet à M. Bellucci qui tout de suite, adore le projet, tout comme sa fille Monica.
        </Text>
        <Text mx={100} my={10} mr={200}>
          L'idée de Gusto-Coffee est sur ses rails.
        </Text>
      </Box>
  );
};


export default Apropos;
