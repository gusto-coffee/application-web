import { Box, Text } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import useCart from '../Hooks/useCart';
import SummaryOrder from '../Components/SummaryOrder';

const UserOrder = ({ match }) => {
  const { getOrder } = useCart();
  const [state, setState] = useState({});
  const [paymentMethod, setPaymentMethod] = useState('Espèce');
  const [dataloaded, setDataloaded] = useState(false);

  useEffect(() => {
    getOrder(match.params.id).then(data => {
      setState(data);
      setDataloaded(true);
      if (data.payment_method === 'bank card') setPaymentMethod('Carte bancaire');
    });
  }, [match.params.id]);

  if (!dataloaded) return <div />;
  return (
    <Box maxW='1150px' m='auto' py='80px' px='30px'>
      <Text fontSize='30px' fontWeight='bold' mb='50px' textAlign='center'>
        {`Commande : ${state.id_order}`}
      </Text>
      <SummaryOrder state={state.shopping_basket_id} />
      <Text mt={4}>{`Montant total TTC : ${state.price}€`}</Text>
      <Text mt={4}>{`Méthode de paiement : ${paymentMethod}`}</Text>
      <Text mt={4} style={{ color: 'silver' }}>
        {state.status === 1 ? 'commande payée' : 'commande non payée'}
      </Text>
    </Box>
  );
};

export default UserOrder;
