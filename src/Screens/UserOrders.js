import { Box, Heading, Stack, Text } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { dateConverterWithHours } from '../DateConverter';
import useCart from '../Hooks/useCart';

const UserOrders = () => {
  const { getOrders } = useCart();
  const [state, setState] = useState([]);
  const [dataloaded, setDataloaded] = useState(false);

  useEffect(() => {
    getOrders().then(data => {
      setState(data);
      setDataloaded(true);
    });
  }, []);

  if (!dataloaded) return <div />;
  return (
    <Stack spacing={8}>
      <Heading m={10} textAlign='center' size='lg'>{`Mes commandes (${state.length})`}</Heading>
      {state.map((order, key) => (
        <Link key={key.toString()} to={`/profil/mes-commandes/${order._id}`} style={{ margin: 10 }}>
          <Box p={5} shadow='md' borderWidth='1px'>
            <Heading as='h3' fontSize='xl'>
              {order.id_order}
            </Heading>
            <div style={{ marginTop: 5, display: 'flex', justifyContent: 'space-between' }}>
              <Text color='silver'>{dateConverterWithHours(order.createdAt)}</Text>
              <Text>{`${order.price}€`}</Text>
            </div>
          </Box>
        </Link>
      ))}
    </Stack>
  );
};

export default UserOrders;
