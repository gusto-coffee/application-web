import {
  Box,
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Text,
  useColorMode,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  Heading,
} from '@chakra-ui/core';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import MetaTags from 'react-meta-tags';
import { useAuth } from '../Hooks/useAuth';

const EditProfile = () => {
  const { user, editMyPassword, editMyProfile } = useAuth();
  const [state, SetState] = useState(user);
  const [success, setSuccess] = useState(false);
  const [formMessage, setFormMessage] = useState({
    show: false,
    message: '',
  });
  const { colorMode } = useColorMode();
  // modal pour modifier son mot de passe
  const [open, setOpen] = useState(false);

  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [passwordSuccess, setPasswordSuccess] = useState(false);
  const [showPasswordError, setShowPasswordError] = useState({
    show: false,
    message: '',
  });
  // messages pour la bonne syntaxe du mot de passe
  const [message, setMessage] = useState([
    {
      passwordLength: 'Le mot de passe doit contenir au moins 10 caractères',
      isValid: false,
    },
    {
      passwordMaj: 'Le mot de passe doit contenir une majuscule',
      isValid: false,
    },
    {
      passwordMin: 'Le mot de passe doit contenir une minuscule',
      isValid: false,
    },
    {
      passwordNum: 'Le mot de passe doit contenir un chiffre',
      isValid: false,
    },
  ]);

  const { handleSubmit, errors, register, formState } = useForm();

  function validateField(value) {
    return value ? true : 'Le champ est requis';
  }

  const validateEmail = value => {
    SetState(prev => ({ prev, email: value }));
    let validationError;
    const emailPattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (!value) {
      validationError = 'Veuillez renseigner un email';
    } else if (!emailPattern.test(value)) {
      validationError = 'Veuillez entrer un email valide.';
    }
    return validationError || true;
  };

  function onSubmit(values) {
    editMyProfile(values).then(data => {
      if (data === true) {
        setSuccess(true);
        setTimeout(() => {
          setSuccess(false);
        }, 2500);
      } else {
        setFormMessage({
          show: true,
          message: data,
        });
      }
    });
  }

  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='1150px' m='auto' py='80px' px='40px' margin='auto'>
        <MetaTags>
          <title>Gusto Coffee : Votre profil</title>
          <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Votre profil' />
          <meta charSet='UTF-8' />
        </MetaTags>
        <Heading size='xl' mb={50} textAlign='center'>
          Modifier mon profil
        </Heading>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl isInvalid={errors.lastname}>
            <FormLabel htmlFor='lastname'>Nom</FormLabel>
            <Input
              name='lastname'
              onChange={({ target }) => SetState(prev => ({ prev, lastname: target.value }))}
              defaultValue={state.lastname}
              ref={register({ validate: validateField })}
            />
            <FormErrorMessage>{errors.lastname && errors.lastname.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={errors.firstname}>
            <FormLabel htmlFor='firstname'>Prénom</FormLabel>
            <Input
              name='firstname'
              onChange={({ target }) => SetState(prev => ({ prev, firstname: target.value }))}
              defaultValue={state.firstname}
              ref={register({ validate: validateField })}
            />
            <FormErrorMessage>{errors.name && errors.firstname.message}</FormErrorMessage>
          </FormControl>
          <FormControl isInvalid={errors.email}>
            <FormLabel htmlFor='email'>Email</FormLabel>
            <Input
              name='email'
              onChange={({ target }) => validateEmail(target.value)}
              defaultValue={state.email}
              ref={register({ validate: validateEmail })}
            />
            <FormErrorMessage>{errors.name && errors.email.message}</FormErrorMessage>
          </FormControl>
          <div style={{ marginTop: '1%', marginBottom: '2.5%' }}>
            <Button variant='link' onClick={() => setOpen(true)}>
              Modifier mon mot de passe
            </Button>
          </div>
          {requestSuccess()}
          <Button mt={4} variantColor='teal' isLoading={formState.isSubmitting} type='submit'>
            Modifier le profil
          </Button>
        </form>

        <Modal closeOnOverlayClick={false} isOpen={open} onClose={() => onClose()}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Modifier mon mot de passe</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <FormControl>
                <FormLabel htmlFor='oldPassword'>Ancien mot de passe</FormLabel>
                <Input type='password' name='newPassword' onChange={({ target }) => setOldPassword(target.value)} value={oldPassword} />
              </FormControl>
              <FormControl isInvalid={message.find(v => v.isValid)}>
                <FormLabel htmlFor='newPassword'>Nouveau mot de passe</FormLabel>
                <Input
                  style={{
                    borderColor: message[0].isValid && message[1].isValid && message[2].isValid && message[3].isValid ? 'green' : 'red',
                  }}
                  type='password'
                  name='newPassword'
                  onChange={({ target }) => checkPassword(target.value)}
                  value={newPassword}
                />
                <Text style={{ padding: 2, fontStyle: 'italic' }}>Le mot de passe doit contenir :</Text>
                <Text style={{ padding: 2, fontStyle: 'italic', color: message[0].isValid ? 'green' : 'red' }}>au moins 10 caractères</Text>
                <Text style={{ padding: 2, fontStyle: 'italic', color: message[1].isValid ? 'green' : 'red' }}>une majuscule</Text>
                <Text style={{ padding: 2, fontStyle: 'italic', color: message[2].isValid ? 'green' : 'red' }}>une minuscule</Text>
                <Text style={{ padding: 2, fontStyle: 'italic', color: message[3].isValid ? 'green' : 'red' }}>un chiffre.</Text>
              </FormControl>

              {passwordRequestSuccess()}
            </ModalBody>

            <ModalFooter>
              <Button variantColor='blue' mr={3} onClick={() => onClose()}>
                Fermer
              </Button>
              <Button isDisabled={message.find(v => !v.isValid)} variant='ghost' onClick={() => passwordSubmit()}>
                Modifier
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </Box>
    </Box>
  );

  function passwordRequestSuccess() {
    return passwordSuccess ? (
      <Alert status='success' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Mise à jour du mot de passe réussie !
        </AlertTitle>
      </Alert>
    ) : showPasswordError.show ? (
      <Alert status='error' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Erreur lors de la mise à jour du mot de passe
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{showPasswordError.message}</AlertDescription>
      </Alert>
    ) : null;
  }

  function requestSuccess() {
    return success ? (
      <Alert status='success' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Informations mises à jour !
        </AlertTitle>
      </Alert>
    ) : formMessage.show ? (
      <Alert status='error' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Erreur lors de la mise à jour des informations
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{formMessage.message}</AlertDescription>
      </Alert>
    ) : null;
  }

  function checkPassword(password) {
    setNewPassword(password);
    const newMessage = message;
    // taille du mot de passe
    if (password.length >= 10) newMessage[0].isValid = true;
    else newMessage[0].isValid = false;
    // au moins un caractère en majuscule
    if (/^(?=.*[A-Z])/.test(password)) newMessage[1].isValid = true;
    else newMessage[1].isValid = false;
    // au moins un caractère en minuscule
    if (/^(?=.*[a-z])/.test(password)) newMessage[2].isValid = true;
    else newMessage[2].isValid = false;
    // au moins un chiffre
    if (/^(?=.*[0-9])/.test(password)) newMessage[3].isValid = true;
    else newMessage[3].isValid = false;

    setMessage(newMessage);
  }

  function passwordSubmit() {
    editMyPassword({ old_password: oldPassword, new_password: newPassword }).then(data => {
      if (data === true) {
        setPasswordSuccess(true);
        setShowPasswordError({
          show: false,
          message: '',
        });
        setTimeout(() => {
          setOpen(false);
        }, 2500);
      } else
        setShowPasswordError({
          show: true,
          message: data,
        });
    });
  }

  function onClose() {
    setOpen(false);
    setPasswordSuccess(false);
    setShowPasswordError({
      show: false,
      message: '',
    });
  }
};

export default EditProfile;
