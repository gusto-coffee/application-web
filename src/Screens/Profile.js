import {
  Box,
  Button,
  useColorMode,
  Stack,
  Heading,
  Avatar,
  Text,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
} from '@chakra-ui/core';
import React, { useState, useEffect } from 'react';
import MetaTags from 'react-meta-tags';
import { Link, Redirect } from 'react-router-dom';
import { dateConverterWithDayWithHours } from '../DateConverter';
import { useAuth } from '../Hooks/useAuth';

const Profile = () => {
  const { getMyProfile, user, removeAccount } = useAuth();
  const [state, setState] = useState({});
  const [isOpen, setIsOpen] = useState(false);
  const { colorMode } = useColorMode();

  useEffect(() => {
    // récupérer le profil pour avoir les points
    if (user)
      getMyProfile().then(response => {
        if (response.status === 200) setState(response.data);
        else alert(response);
      });
  }, [user]);

  const Feature = ({ title, to, buttonTitle, buttonColor, onClick }) =>
    // si le bouton n'est pas un lien (supprimer son compte)
    to ? (
      <Box p={5} mb={5} shadow='md' borderWidth='1px' rounded='md' display='flex' alignItems='center' justifyContent='space-between'>
        <Heading fontSize='xl'>{title}</Heading>
        <Link to={to}>
          <Button variantColor={buttonColor} w={150}>
            {buttonTitle}
          </Button>
        </Link>
      </Box>
    ) : (
      <Box p={5} mb={5} shadow='md' borderWidth='1px' rounded='md' display='flex' alignItems='center' justifyContent='space-between'>
        <Heading fontSize='xl'>{title}</Heading>
        <Button w={150} variantColor={buttonColor} onClick={onClick}>
          {buttonTitle}
        </Button>
      </Box>
    );

  if (!user) return <Redirect to='/login' />;
  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='1150px' m='auto' py='80px' px='40px' margin='auto'>
        <MetaTags>
          <title>Gusto Coffee : Votre profil</title>
          <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Votre profil' />
          <meta charSet='UTF-8' />
        </MetaTags>
        <div style={{ textAlign: 'center' }}>
          <Avatar alignContent='center' size='2xl' name={`${state.firstname} ${state.lastname}`} color='white' backgroundColor='#3A2F2F' mb={50} />
        </div>
        <Text mb={1} textAlign='right' color='silver'>{`Inscrit depuis le ${dateConverterWithDayWithHours(state.createdAt)}`}</Text>

        <Stack spacing={8}>
          <Feature title='Modifier mon profil' to='/profil/modifier-mon-profil' buttonTitle='Modifier' />
          <Feature title='Utiliser mes points' to='/profil/utiliser-mes-points' buttonTitle={state.fidelity_points} />
          <Feature title='Voir mes commandes' to='/profil/mes-commandes' buttonTitle='Voir' />
          <Feature title='Supprimer mon compte' onClick={() => setIsOpen(true)} buttonTitle='Supprimer' buttonColor='red' />
        </Stack>
        {modalDeleteUser()}
      </Box>
    </Box>
  );

  function deleteAccount() {
    removeAccount().then(data => {
      if (data === true) setIsOpen(false);
      else window.alert(data);
    });
  }

  function modalDeleteUser() {
    return (
      <AlertDialog isOpen={isOpen} onClose={() => setIsOpen(false)}>
        <AlertDialogOverlay />
        <AlertDialogContent>
          <AlertDialogHeader fontSize='lg' fontWeight='bold'>
            Suppression compte utilisateur
          </AlertDialogHeader>

          <AlertDialogBody>Voulez-vous supprimer votre compte ?</AlertDialogBody>

          <AlertDialogFooter>
            <Button onClick={() => setIsOpen(false)}>Annuler</Button>
            <Button variantColor='red' onClick={() => deleteAccount()} ml={3}>
              Supprimer
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialog>
    );
  }
};

export default Profile;
