import { Box, Button, Heading, Stack, Text } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';
import useCart from '../Hooks/useCart';
import { socket as io } from '../SocketIO';

const Payment = ({ match }) => {
  const [socket, setSocket] = useState(null);
  const [, , removeCookie] = useCookies(['shoppingBasket', 'reservation']);

  const { getBasket, confirmPayment } = useCart();
  const [state, setState] = useState({});
  const [dataloaded, setDataloaded] = useState(false);
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    setSocket(io);
    getBasket(match.params.id).then(data => {
      setState(data.basket);
      setDataloaded(true);
    });
  }, [match.params.id]);

  if (redirect) return <Redirect to='/' />;
  if (!dataloaded) return <div />;
  return (
    <Box maxW='1150px' m='auto' py='80px' px='30px'>
      <Text fontSize='30px' fontWeight='bold' mb='50px' textAlign='center'>
        Récapitulatif de commande
      </Text>
      <Stack spacing={8} mb='50px'>
        <Text fontSize='26px' fontWeight='bold' mb='30px'>
          Produits
        </Text>
        {state.articles && state.articles.length ? (
          state.articles.map(article => (
            <Box p={5} shadow='md' borderWidth='1px'>
              <Heading mb='5px' fontSize='xl'>
                ({article.quantity}) {article.product_id.name}
              </Heading>
              <Text mt={4}>{`Sous-total : ${article.price}€`}</Text>
              <Text fontSize='12px'>{`Dont TVA : ${(article.price * 10) / 100}€`}</Text>
            </Box>
          ))
        ) : (
          <Text>Aucun produit.</Text>
        )}
      </Stack>
      <Stack spacing={8} my='50px'>
        <Text fontSize='26px' fontWeight='bold' mb='30px'>
          Réservations
        </Text>
        {state.reservations &&
          state.reservations.map(reservation => (
            <Box p={5} shadow='md' borderWidth='1px'>
              <Heading mb='5px' fontSize='xl'>
                Salon {reservation.reservation_id.location_id.name}
              </Heading>
              <Text mt={4}>
                Date : {String(reservation.reservation_id.day).padStart(2, '0')}/{String(reservation.reservation_id.month).padStart(2, '0')}/
                {reservation.reservation_id.year}
              </Text>
              <Text>Heure de début : {reservation.reservation_id.start}h</Text>
              <Text>Heure de fin : {reservation.reservation_id.end}h</Text>
              <Text mt={4}>Sous-total : {reservation.price}€</Text>
              <Text fontSize='12px'>{`Dont TVA : ${(reservation.price * 20) / 100}€`}</Text>
            </Box>
          ))}
      </Stack>
      <Text mt={4}>{`Montant total : ${state.total_price}€`}</Text>
      <Button onClick={() => handleClick()} display='block' ml='auto' mt='50px' backgroundColor='marronClair' color='white'>
        Valider le paiement
      </Button>
    </Box>
  );

  function handleClick() {
    confirmPayment(match.params.id).then(data => {
      // si la personne est connecté
      socket.emit('new-order', { _id: data._id });

      // supprimer le stockge local
      localStorage.removeItem('basket');
      removeCookie('shoppingBasket');
      removeCookie('reservation');
      localStorage.setItem('confirmBasket', JSON.stringify(0));
      alert('Merci pour votre paiement !');
      setTimeout(() => {
        setRedirect(true);
      }, 1500);
    });
  }
};

export default Payment;
