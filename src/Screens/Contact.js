import { Box, Button, FormControl, FormLabel, Heading, Input, Text, Textarea, useColorMode } from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';

const ContactUs = () => {
  const { colorMode } = useColorMode();
  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='1150px' m='auto' py='80px'>
        <MetaTags>
          <title>Gusto Coffee : Nous contacter</title>
          <meta
            name='description'
            content='Gusto Coffee : Une entreprise fort de café ! Une question, une remarque ou un message à nous faire passer ? Nous sommes à votre écoute !'
          />
        </MetaTags>
        <Box maxWidth='80%' mx='auto'>
          <Heading as='h2' mb='50px'>
            Contactez nous !
          </Heading>
          <Text mb='80px'>
            Une question, une remarque ou un message à nous faire passer ? Nous sommes à votre écoute ! Remplissez le formulaire ci-dessous.Nous
            traiterons votre demande dans un délai de 24 à 48 heures.
          </Text>
          <FormControl display='flex' flexDirection='column' mb='20px' isRequired>
            <FormLabel htmlFor='email'>Email</FormLabel>
            <Input type='email' id='email' backgroundColor='white' />
          </FormControl>
          <FormControl display='flex' flexDirection='column' mb='20px' isRequired>
            <FormLabel htmlFor='text'>Sujet</FormLabel>
            <Input type='text' id='sujet' backgroundColor='white' />
          </FormControl>
          <FormControl display='flex' flexDirection='column' mb='20px' isRequired>
            <FormLabel htmlFor='message'>Message</FormLabel>
            <Textarea size='sm' backgroundColor='white' />
          </FormControl>
          <Button
            mt={10}
            backgroundColor='marronClair'
            color='white'
            type='submit'
            display='block'
            marginLeft='auto'
            _hover={{ backgroundColor: 'marronClair', color: 'marron' }}>
            Envoyer
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default ContactUs;
