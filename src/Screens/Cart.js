import { Box, Button, Flex, Image, Link, Stack, Text, useColorMode } from '@chakra-ui/core';
import React, { useState } from 'react';
import { MetaTags } from 'react-meta-tags';
import { Link as RouterLink, Redirect } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import Feature from '../Components/Feature';
import { useAuth } from '../Hooks/useAuth';
import useCart from '../Hooks/useCart';

const Cart = () => {
  const [cookie] = useCookies(['shoppingBasket', 'reservation']);

  const { confirmCart, removeProductToCart, removeReservationFromCart } = useCart();
  const [redirect, setRedirect] = useState(false);
  const { user } = useAuth();
  const { colorMode } = useColorMode();

  if (redirect && localStorage.getItem('basket') !== null) return <Redirect to={`/panier/${JSON.parse(localStorage.getItem('basket'))}/paiement`} />;

  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <MetaTags>
        <title>Gusto Coffee : Votre panier</title>
        <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Votre panier.' />
        <meta charSet='UTF-8' />
      </MetaTags>
      <Box maxW='1150px' m='auto' py='80px' px='30px'>
        <Text fontSize='30px' fontWeight='bold' mb='50px' textAlign='center'>
          Panier
        </Text>
        <Stack spacing={8} mb='50px'>
          <Text fontSize='26px' fontWeight='bold' mb='30px'>
            Produits
          </Text>
          {cookie.shoppingBasket ? (
            cookie.shoppingBasket.length > 0 ? (
              cookie.shoppingBasket.map((article, key) => (
                <Feature
                  removeProductToCart={removeProductToCart}
                  key={key.toString()}
                  title={article.name}
                  price={article.totalPrice}
                  article={article}
                  index={key}
                />
              ))
            ) : (
              <Text>Aucun produit dans le panier</Text>
            )
          ) : (
            <Text>Aucun produit dans le panier</Text>
          )}
        </Stack>
        <Stack spacing={8} mb='50px'>
          <Text fontSize='26px' fontWeight='bold' mb='30px'>
            Réservations
          </Text>
          {cookie.reservation ? (
            cookie.reservation.length > 0 ? (
              <>
                {cookie.reservation.map((reservation, key) => (
                  <Flex key={key.toString()} alignItems='center' mb='50px' flexWrap='wrap' p={5} shadow='md' borderWidth='1px'>
                    <Image src={require(`../assets/images/salon-${reservation.location_id.name}.jpg`)} maxW='300px' />
                    <Flex
                      flexDirection='column'
                      ml={{ xs: '0', sm: '50px', md: '50px', lg: '50px', xl: '50px' }}
                      justifyContent='center'
                      mt={{ xs: '50px' }}
                      mb='50px'>
                      <Text fontSize='18px' fontWeight='500' mb='20px'>
                        Salon {reservation.location_id.name}
                      </Text>
                      <Text>
                        Date : {String(reservation.reservation_id.day).padStart(2, '0')}/{String(reservation.reservation_id.month).padStart(2, '0')}/
                        {String(reservation.reservation_id.year).padStart(2, '0')}
                      </Text>
                      <Text>Heure de début : {String(reservation.reservation_id.start).padStart(2, '0')}h</Text>
                      <Text>Heure de fin : {String(reservation.reservation_id.end).padStart(2, '0')}h</Text>
                      <Text>Prix : {String(reservation.totalPrice).padStart(2, '0')}€</Text>
                    </Flex>
                    <Button
                      ml='auto'
                      mt='auto'
                      display='block'
                      backgroundColor='marronClair'
                      color='white'
                      size='sm'
                      _hover={{ opacity: '0.7' }}
                      onClick={() => removeReservationFromCart(reservation, key)}>
                      Supprimer
                    </Button>
                  </Flex>
                ))}
              </>
            ) : (
              <Text>Aucune reservation dans le panier</Text>
            )
          ) : (
            <Text>Aucune reservation dans le panier</Text>
          )}
        </Stack>
        {!user ? (
          <Text marginLeft='auto'>
            Veuillez{' '}
            <Link as={RouterLink} to='/login' cursor='pointer' _focus={{ boxShadow: 'none' }} style={{ color: 'green' }}>
              vous connecter
            </Link>{' '}
            pour pouvoir valider votre panier
          </Text>
        ) : (
          <Button onClick={() => validateCart()} display={displayButton() ? 'block' : 'none'} ml='auto' mt='50px' variantColor='green'>
            Valider mon panier
          </Button>
        )}
      </Box>
    </Box>
  );

  function displayButton() {
    if (cookie.reservation) {
      if (cookie.reservation.length > 0) return true;
    }
    if (cookie.shoppingBasket) {
      if (cookie.shoppingBasket.length > 0) return true;
    }
    return false;
  }
  function validateCart() {
    confirmCart().then(data => {
      // ajouter une condition pour vérifier que l'utilisateur est bien inscrit
      if (data === true) setRedirect(true);
    });
  }
};

export default Cart;
