import { Box, Button } from '@chakra-ui/core';
import React from 'react';
import { useLocation } from 'react-router-dom';
import useCart from '../Hooks/useCart';

const ProductDetail = () => {
  const { state } = useLocation();
  const { addProductToCart, removeProductToCart } = useCart();

  return (
    <>
      <Box p='150px'>
        <pre>{JSON.stringify(state.product, null, '\t')}</pre>
      </Box>
      <Button onClick={() => addProductToCart(state.product)}>Ajouter</Button>
      <Button onClick={() => removeProductToCart(state.product)}>Supprimer</Button>
    </>
  );
};

export default ProductDetail;
