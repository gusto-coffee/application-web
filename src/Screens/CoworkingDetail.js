import { Box, Button, Flex, Image, Select, Text, useColorMode, useToast } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import MetaTags from 'react-meta-tags';
import { useLocation } from 'react-router-dom';
import { useAuth } from '../Hooks/useAuth';
import useCart from '../Hooks/useCart';
import { getDayReservation } from '../utils/API';
import { hoursValue } from '../utils/reservationHours';

const CoworkingDetail = () => {
  const { state } = useLocation();
  const { user } = useAuth();
  const { colorMode } = useColorMode();
  const toast = useToast();

  const { addReservationToCart } = useCart();
  const [reservationError, setReservationError] = useState(null);

  const [date, setDate] = useState(new Date());
  const [heureDebut, setHeureDebut] = useState('');
  const [heureFin, setHeureFin] = useState('');
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const [reservedHours, setReservedHours] = useState([]);

  const handleChangeCalendar = newDate => {
    setDate(newDate);
  };

  useEffect(() => {
    const temporaryStartArray = [];
    const temporaryEndArray = [];
    getDayReservation(state.coworking._id, day, month, year).then(({ data }) => {
      // Si on a des reservations
      if (data.length) {
        // On parcourt notre tableau de réservation
        data.forEach(reservation => {
          // On parcourt le tableau de modèle d'heures
          hoursValue.forEach(hour => {
            // Si l'heure du modele est supérieure à l'heure courante
            if (hour.value > date.getHours()) {
              // Si l'heure du modele est plus petite que l'heure l'heure fin de la reservation
              if (hour.value < reservation.end) {
                // On affiche que l'emplacement est deja reservé a cette heure
                temporaryStartArray.push({ label: `${hour.value}h - Emplacement déja reservé`, value: 'reserve' });
              } else {
                // Sinon on affiche l'heure
                temporaryStartArray.push({ label: `${hour.value}h`, value: hour.value });
              }

              // Si l'heure du modele est plus grande ou égale a l'heure de début de réservation
              // et plus petite ou égale a l'heure de fin de réservation
              if (hour.value >= reservation.start && hour.value <= reservation.end) {
                // On affiche que l'emplacement est deja reservé a cette heure
                temporaryEndArray.push({ label: `${hour.value}h - Emplacement déja reservé`, value: 'reserve' });
              } else {
                // Sinon on affiche l'heure
                temporaryEndArray.push({ label: `${hour.value}h`, value: hour.value });
              }
            }
          });
        });
        setReservedHours({ start: temporaryStartArray, end: temporaryEndArray });
        // Sinon si on a pas de reservation sur cet espace
      } else {
        // On parcourt notre modèle d'heure
        hoursValue.forEach(hour => {
          // On affiche les heures qui ne sont pas passées
          if (hour.value > date.getHours()) {
            temporaryStartArray.push({ label: `${hour.value}h`, value: hour.value });
            temporaryEndArray.push({ label: `${hour.value}h`, value: hour.value });
            // setReservedHours({ start: temporaryStartArray, end: temporaryEndArray });
          }
        });
        setReservedHours({ start: temporaryStartArray, end: temporaryEndArray });
      }
    });
  }, [date]);

  // Méthode permettant d'ajouter une réservation au panier
  const makeReservation = () => {
    if (!user) {
      setReservationError(
        'Vous devez vous connecter afin de pouvoir réserver cet espace. Nous avons mis en place cette sécurité afin que personne ne puisse réserver le meme espace que vous le temps que vous validiez votre panier.',
      );
    } else if (heureDebut === '' || heureFin === '') {
      setReservationError('Veuillez séléctionner un horaire de début et de fin.');
    } else if (heureDebut === 'reserve' || heureFin === 'reserve') {
      setReservationError('Les horaires séléctionnés sont déja réservés.');
    } else {
      setReservationError(null);
      const response = addReservationToCart({
        user_id: user.id,
        location_id: state.coworking._id,
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
        start: +heureDebut,
        end: +heureFin,
        points: state.coworking.location_category_id.points,
        prix: state.coworking.location_category_id.price,
        name: state.coworking.name,
        location_category_id: state.coworking.location_category_id._id,
      });
      if (response === 'erreur') {
        setReservationError("Erreur lors de la réservation d'un espace");
      } else {
        toast(
          {
            title: 'Réservation ajoutée au panier.',
            status: 'success',
            duration: 2000,
            isClosable: true,
          },
          state.coworking,
        );
      }
    }
  };

  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxWidth='1400px' mx='auto' px='20px' py='50px'>
        <MetaTags>
          <title>Gusto Coffee : Détails de la salle</title>
          <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Détails concernant nos salles de Coworking Gusto Coffee.' />
          <meta charet='UTF-8' />
        </MetaTags>
        <Text fontSize='26px' fontWeight='bold' mb='50px' maxWidth='1150px' mx='auto'>
          Salon {state.coworking.name}
        </Text>
        <Flex justifyContent='space-around' flexWrap='wrap'>
          <Flex flexDirection='column' maxW='800px'>
            <Flex justifyContent='space-around' flexWrap='wrap'>
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
              <Image maxW='300px' src={require(`../assets/images/salon-${state.coworking.name}.jpg`)} mb='30px' />
            </Flex>
          </Flex>
          <Flex flexDirection='column' alignItems='center'>
            <Calendar value={date} onChange={handleChangeCalendar} activeStartDate={date} />
            <Flex justifyContent='space-between' alignItems='center'>
              <Select
                placeholder='Heure de début'
                mt='50px'
                mr='10px'
                onChange={({ target }) => setHeureDebut(target.value)}
                fontSize='14px'
                w='180px'>
                {reservedHours.start
                  ? reservedHours.start.map((hour, index) => {
                      if (index !== reservedHours.start.length - 1) {
                        return (
                          <option value={hour.value} key={index.toString()}>
                            {hour.label}
                          </option>
                        );
                      }
                    })
                  : reservedHours.map((hour, index) => {
                      if (index !== reservedHours.length - 1) {
                        return (
                          <option value={hour.value} key={index.toString()}>
                            {hour.label}
                          </option>
                        );
                      }
                    })}
              </Select>
              <Select placeholder='Heure de fin' mt='50px' onChange={({ target }) => setHeureFin(target.value)} fontSize='14px' w='180px'>
                {reservedHours.end
                  ? reservedHours.end
                      .filter((hour, index) => index !== 0)
                      .map((hour, index) => (
                        <option value={hour.value} key={index.toString()} style={{ color: 'black' }}>
                          {hour.label}
                        </option>
                      ))
                  : reservedHours
                      .filter((hour, index) => index !== 0)
                      .map((hour, index) => (
                        <option value={hour.value} key={index.toString()} style={{ color: 'black' }}>
                          {hour.label}
                        </option>
                      ))}
              </Select>
            </Flex>
          </Flex>
          {reservationError && (
            <Text fontSize='13px' style={{ color: 'red' }} mt='18px' textAlign='center' maxW='70%'>
              {reservationError}
            </Text>
          )}
        </Flex>
        <Flex justifyContent='space-between' px='80px' alignItems='end' mt='30px' flexWrap='wrap'>
          <Flex flexDirection='column'>
            <Text>Nombre de places : {state.coworking.location_category_id.place_number}</Text>
            <Text>Prix : {state.coworking.location_category_id.price}€/heure</Text>
            <Text>Equipements : {state.coworking.location_category_id.stuff}</Text>
          </Flex>
          <Button
            display='block'
            ml='auto'
            mt='50px'
            mr='50px'
            backgroundColor='marronClair'
            _disabled={{ opacity: '0.8' }}
            color='white'
            onClick={makeReservation}>
            Réserver cet espace
          </Button>
        </Flex>
      </Box>
    </Box>
  );
};

export default CoworkingDetail;
