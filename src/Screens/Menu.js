import { Box, Heading, useColorMode } from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';
import ProductList from '../Components/ProductList';

const Menu = () => {
  const { colorMode } = useColorMode();
  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='1150px' m='auto' py='80px' px={10} margin='auto'>
        <MetaTags>
          <title>Gusto Coffee : Carte de nos produits</title>
          <meta
            name='description'
            content='Gusto Coffee : Une entreprise fort de café ! Découvrez la liste de nos produits et services Gusto Coffee.s'
          />
          <meta charSet='UTF-8' />
        </MetaTags>
        <Heading as='h2' mb='80px'>
          Nos produits
        </Heading>
        <ProductList />
      </Box>
    </Box>
  );
};

export default Menu;
