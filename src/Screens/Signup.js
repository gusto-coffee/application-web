import React from 'react';
import { Flex } from '@chakra-ui/core';
import MetaTags from 'react-meta-tags';
import SignupForm from '../Components/SignupForm';

const Signup = () => (
  <>
    <MetaTags>
      <title>Gusto Coffee : S'enregistrer</title>
      <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Rejoignez nous !' />
      <meta charSet='UTF-8' />
    </MetaTags>
    <Flex h='80vh' alignItems='center' justify='center' direction='column' lang='fr'>
      <h2>Inscription</h2>
      <SignupForm />
    </Flex>
  </>
);

export default Signup;
