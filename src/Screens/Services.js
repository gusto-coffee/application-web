import { Box, Flex, Heading, Link, Text, useColorMode } from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';
import { Link as RouterLink } from 'react-router-dom';
import salleCommune from '../assets/images/salle-commune.jpg';
import salleMoka from '../assets/images/salon-Moka.jpg';
import serviceBoissons from '../assets/images/service-boissons.jpg';
import serviceCafe from '../assets/images/service-cafe.jpg';
import serviceInformatique from '../assets/images/service-informatique.jpg';
import serviceRestauration from '../assets/images/service-restauration.jpg';

const Services = () => {
  const { colorMode } = useColorMode();
  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='1350px' m='auto' py='80px' px={10} margin='auto'>
        <MetaTags>
          <title>Gusto Coffee : Nos services.</title>
          <meta
            name='description'
            content='Gusto Coffee : Une entreprise fort de café ! Réservez votre place, votre café et vos petites gourmandises !'
          />
          <meta charSet='UTF-8' />
        </MetaTags>
        <Heading as='h2' mb='30px'>
          Nos services
        </Heading>
        <Text mb='50px' fontSize='18px'>
          Retrouvez la liste des différents services proposés par Gusto Coffee.
        </Text>
        <Flex alignItems='top' justifyContent={{ sm: 'center', md: 'space-around', lg: 'space-around', xl: 'space-between' }} flexWrap='wrap'>
          <Flex flexDirection='column' flex='0 1 350px' mb='80px'>
            <Link
              as={RouterLink}
              to='/coworking'
              display='flex'
              background={`url(${salleMoka})`}
              backgroundPosition='center center'
              backgroundSize='cover'
              backgroundRepeat='no-repeat'
              minHeight='300px'
              alignItems='center'
              justifyContent='center'
              mb='30px'
              _hover={{ textDecoration: 'none' }}>
              <Text
                backgroundColor='rgba(255, 255, 255, 0.84)'
                px='20px'
                py='6px'
                borderRadius='30px'
                fontWeight='bold'
                fontSize='22px'
                color='marron'>
                Espace de coworking
              </Text>
            </Link>
            <Text fontSize='17px' fontWeight='600' mb='10px'>
              Réservez votre salon privatif pour vos réunions.
            </Text>
            <Text>
              Vous aurez accès à une connexion Internet haut débit, différents équipements selon le salon choisi, un environnement de travail agréable
              et confortable. Au choix : café et thé à volonté, snacks et services informatiques (profitez de 10% de réduction si vous si vous
              commandez en même temps que votre réservation).
              <strong> La 3ème heure offerte de 9h à 16h.</strong>
            </Text>
          </Flex>
          <Flex flexDirection='column' flex='0 1 350px' mb='80px'>
            <Link
              as={RouterLink}
              to='/coworking#salle-commune'
              display='flex'
              background={`url(${salleCommune})`}
              backgroundPosition='center center'
              backgroundSize='cover'
              backgroundRepeat='no-repeat'
              minHeight='300px'
              alignItems='center'
              justifyContent='center'
              mb='30px'
              _hover={{ textDecoration: 'none' }}>
              <Text
                backgroundColor='rgba(255, 255, 255, 0.84)'
                px='20px'
                py='6px'
                borderRadius='30px'
                fontWeight='bold'
                fontSize='22px'
                color='marron'>
                Salle commune
              </Text>
            </Link>
            <Text fontSize='17px' fontWeight='600' mb='10px'>
              Réservez votre place dans notre espace de Coworking Gusto Coffee.
            </Text>
            <Text>
              Vous aurez accès à une connexion Internet haut débit par Wifi, un environnement de travail agréable et confortable. Au choix : café et
              thé à volonté, snacks et services informatiques (profitez de 10% de réduction si vous si vous commandez en même temps que votre
              réservation).
              <strong> La 3ème heure offerte de 9h à 16h.</strong>
            </Text>
          </Flex>
          <Flex flexDirection='column' flex='0 1 350px' mb='80px'>
            <Flex background={`url(${serviceInformatique})`} minHeight='300px' alignItems='center' justifyContent='center' mb='30px'>
              <Text
                backgroundColor='rgba(255, 255, 255, 0.84)'
                px='20px'
                py='6px'
                borderRadius='30px'
                fontWeight='bold'
                fontSize='22px'
                color='marron'>
                Informatique
              </Text>
            </Flex>
            <Text fontSize='17px' fontWeight='600' mb='10px'>
              Besoin d'imprimer un document ? ou de solutions de stockage pour vos données ?
            </Text>
            <Text>
              Nous avons ce qu'il vous faut ! Vous avez la possibilité de commander directement au comptoir du café ou bien depuis notre application
              mobile et profitez de 10% de réduction si vous si vous commandez en même temps que votre réservation !
            </Text>
          </Flex>
          <Flex flexDirection='column' flex='0 1 350px' mb='80px'>
            <Link
              as={RouterLink}
              to='/la-carte'
              display='flex'
              background={`url(${serviceCafe})`}
              backgroundPosition='center center'
              backgroundSize='cover'
              backgroundRepeat='no-repeat'
              minHeight='300px'
              alignItems='center'
              justifyContent='center'
              _hover={{ textDecoration: 'none' }}
              mb='30px'>
              <Text
                backgroundColor='rgba(255, 255, 255, 0.84)'
                px='20px'
                py='6px'
                borderRadius='30px'
                fontWeight='bold'
                fontSize='22px'
                color='marron'>
                Boissons chaudes
              </Text>
            </Link>
            <Text fontSize='17px' fontWeight='600' mb='10px'>
              Un petit coup de fatigue ou besoin d'un peu de réconfort ?
            </Text>
            <Text>
              Laissez-vous tentez par nos <em>délicieux cafés, thés et chocolats chauds</em> ! Vous avez la possibilité de commander directement au
              comptoir du café ou bien depuis notre application mobile et profitez de 10% de réduction si vous si vous commandez en même temps que
              votre réservation !
            </Text>
          </Flex>

          <Flex flexDirection='column' flex='0 1 350px' mb='80px'>
            <Link
              as={RouterLink}
              to='/la-carte'
              display='flex'
              background={`url(${serviceBoissons})`}
              backgroundPosition='center center'
              backgroundSize='cover'
              backgroundRepeat='no-repeat'
              minHeight='300px'
              alignItems='center'
              justifyContent='center'
              _hover={{ textDecoration: 'none' }}
              mb='30px'>
              <Text
                backgroundColor='rgba(255, 255, 255, 0.84)'
                px='20px'
                py='6px'
                borderRadius='30px'
                fontWeight='bold'
                fontSize='22px'
                color='marron'>
                Boissons froides
              </Text>
            </Link>
            <Text fontSize='17px' fontWeight='600' mb='10px'>
              Envie de se rafraîchir ?
            </Text>
            <Text>
              Venez dégustez nos <em>jus de fruits frais, sodas, smoothies</em> et autres spécialités ! Vous avez la possibilité de commander
              directement au comptoir du café ou bien depuis notre application mobile et profitez de 10% de réduction si vous si vous commandez en
              même temps que votre réservation !
            </Text>
          </Flex>

          <Flex flexDirection='column' flex='0 1 350px' mb='80px'>
            <Link
              as={RouterLink}
              to='/la-carte'
              display='flex'
              background={`url(${serviceRestauration})`}
              backgroundPosition='center center'
              backgroundSize='cover'
              backgroundRepeat='no-repeat'
              minHeight='300px'
              alignItems='center'
              justifyContent='center'
              mb='30px'
              _hover={{ textDecoration: 'none' }}>
              <Text
                backgroundColor='rgba(255, 255, 255, 0.84)'
                px='20px'
                py='6px'
                borderRadius='30px'
                fontWeight='bold'
                fontSize='22px'
                color='marron'>
                Snacks
              </Text>
            </Link>
            <Text fontSize='17px' fontWeight='600' mb='10px'>
              Un petit creux ?
            </Text>
            <Text>
              Laissez-vous tentez par nos snacks élaborés par nos chefs en cuisine ! Comme le <em>Chicago's Hot dog</em>, nos délicieuses crêpes et
              gaufres ! Votre estomac vous remerciera ! Vous avez la possibilité de commander directement au comptoir du café ou bien depuis notre
              application mobile et profitez de 10% de réduction si vous si vous commandez en même temps que votre réservation !
            </Text>
          </Flex>
        </Flex>
      </Box>
    </Box>
  );
};

export default Services;
