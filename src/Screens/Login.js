import { Box, Heading, useColorMode } from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';
import LoginForm from '../Components/LoginForm';

const Login = () => {
  const { colorMode } = useColorMode();

  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <Box maxW='950px' mx='auto' px='100px' py='120px' m='auto'>
        <MetaTags>
          <title>Gusto Coffee : connexion</title>
          <meta name='description' content='Gusto Coffee : Une entreprise fort de café ! Merci de vous connecter pour accéder à nos services.' />
          <meta charSet='UTF-8' />
        </MetaTags>
        <Heading as='h2' mb='50px'>
          Connexion
        </Heading>
        <LoginForm />
      </Box>
    </Box>
  );
};

export default Login;
