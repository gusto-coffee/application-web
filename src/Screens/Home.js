import { Box, Button, Flex, Heading, Image, Link, List, ListItem, Text, useColorMode } from '@chakra-ui/core';
import React from 'react';
import MetaTags from 'react-meta-tags';
import { Link as RouterLink } from 'react-router-dom';
import googleMap from '../assets/images/carte-gare-du-nord.jpg';
import emmaFemal from '../assets/images/emma-femal.jpg';
import jeanGular from '../assets/images/jean-gular.jpg';
import marinDodousse from '../assets/images/marin-dodousse.jpg';
import salleCommune from '../assets/images/salle-commune-big.jpg';

const Home = () => {
  const { colorMode } = useColorMode();
  return (
    <Box backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
      <MetaTags>
        <title>Gusto Coffee : Café et Coworking</title>
        <meta
          name='description'
          content="Gusto Coffee : Une entreprise fort de café ! Coffee shop - Coworking | Situé a Paris près de la gare du nord, venez travailler dans un coworking agréable et chaleureux, tout en profitant de cafés d'exception !"
        />
      </MetaTags>
      <Flex flexDirection='column' maxWidth='1350px' mx='auto' py='80px' px='50px'>
        <Heading as='h2' mb='50px' size='2xl'>
          Notre concept
        </Heading>
        <Heading as='h2' mt='40px' fontSize='17px'>
          Qu'est-ce qu'un Café Coworking ?
        </Heading>
        <Text my={10}>
          Entre simple <em>café de quartier</em> et
          <em>espace de coworking</em>, les cafés coworking sont des lieux hybrides qui fonctionnent comme un café classique mais où l’on paie au
          temps passé. Généralement plus calmes qu’un bar de quartier, ils mettent à la disposition des « sans bureau fixe » tout le nécessaire pour
          <strong> travailler convenablement</strong> tout en joignant l’utile à l’agréable : du
          <em>bon café et de petites gourmandises</em> pour affronter une grosse journée de travail.
        </Text>
        <Heading as='h2' mt='40px' fontSize='16px'>
          <strong>Gusto Coffee</strong>, le Café Coworking où il fait bon travailler !
        </Heading>
        <Text my={10}>
          Situé à deux pas de la <strong>Gare du Nord</strong>, notre équipe met tout en oeuvre pour que vous vous sentiez dans nos locaux comme chez
          vous. Dans un cadre agréable et moderne, vous profiterez d'une connexion rapide grâce à notre connexion fibre ultra-performante, d'une
          communauté dynamique et bienveillante et surtout de notre fameux café, reconnu internationalement, dont seul
          <strong> Gusto Coffee</strong> a le secret.
        </Text>
        <Heading as='h2' mt='40px' fontSize='16px'>
          Les Plus <strong>Gusto Coffee</strong>
        </Heading>
        <Text mt={10}>En plus de nos locaux, vous pourrez profiter :</Text>
        <Flex justifyContent='space-between' my={10} flexWrap='wrap'>
          <List>
            <ListItem>De notre séléction de produits numériques disponibles à la vente.</ListItem>
            <ListItem>De l'accès au channel Slack de Gusto Coffee</ListItem>
            <ListItem>De notre savoir-faire en matière de café à travers la carte des spécialtés.</ListItem>
          </List>
          <List>
            <ListItem>D'un espace de stockage numérique sécurisé.</ListItem>
            <ListItem>D'un service d'impression rapide et de qualité.</ListItem>
            <ListItem>De boissons chaudes Gusto Coffee à volonté (Café et thé).</ListItem>
          </List>
        </Flex>
      </Flex>

      <Box height='450px' background={`url(${salleCommune})`} objectFit='contain' backgroundPosition='bottom center'>
        <Flex
          backgroundColor='rgba(255, 255, 255, 0.84)'
          flexDirection='column'
          width={{ xs: '100%', sm: '65%', md: '50%' }}
          height='100%'
          justifyContent='center'
          color='black'
          px='50px'>
          <Heading as='h2' fontSize='20px' mb='50px'>
            Tarifs coworking
          </Heading>
          <Text mb='10px'>Espace commun : 4€ / Heure</Text>
          <Text mb='10px'>Salle 4 personnes : 120€ / Demi-journée</Text>
          <Text mb='50px'>Salle 6 personnes : 140€ / Demi-journée</Text>
          <Text>Cafés, viennoiseries et service informatique disponible</Text>
          <Button display='block' ml='auto' mt='30px' backgroundColor='marronClair' color='white' _hover={{ backgroundColor: 'marronClair' }}>
            <Link as={RouterLink} to='/coworking' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }}>
              Voir les espaces
            </Link>
          </Button>
        </Flex>
      </Box>

      <Flex bg='marron' justifyContent='space-around' pt='60px' pb='120px' flexDirection='column'>
        <Heading as='h3' mb='70px' px='120px' color='white'>
          Retrouvez nous
        </Heading>
        <Image src={googleMap} height='430px' objectFit='cover' />
      </Flex>
      <Flex flexDirection='column' alignItems='center' backgroundColor='marronClair'>
        <Box maxWidth={{ xs: '1350px', sm: '1350px', md: '850px' }} mx='auto' py='80px' px={10}>
          <Heading as='h3' textAlign='center' mt='30px' color='white' mb='80px'>
            Les valeurs de Gusto Coffee
          </Heading>
          <Text my={10} color='white'>
            En tant qu'amoureux du café, nous mettons un point d'honneur a sélectionner nous mêmes les variétés de cafés et nos producteurs. Nous les
            rémunérons de façon équitable, afin qu'ils puissent vivre dignement de leur travail.
          </Text>
          <Text my={10} color='white'>
            Dans une logique de développement durable, notre site web et notre application mobile sont développés de manière à respecter
            l'environnement et à consommer le moins d'énérgie et de données possibles.
          </Text>
          <Text my={10} color='white'>
            Afin de lutter contre le gaspillage, nos denrées alimentaires périssables invendues sont redistribuées grâcieusement à des associations
            venant en aide à des personnes dans le besoin.
          </Text>
        </Box>
      </Flex>
      <Flex
        alignItems='center'
        py='80px'
        flexWrap='wrap'
        flexDirection='column'
        maxWidth='1350px'
        mx='auto'
        backgroundColor={colorMode === 'light' ? 'white' : 'marron'}>
        <Heading as='h3' mb='80px'>
          Ce que pensent nos clients
        </Heading>
        <Flex flexWrap='wrap' justifyContent='space-between'>
          <Flex flexDirection='column' flex='0 1 350px' mx='auto' mb='50px'>
            <Flex alignItems='center' mb='30px'>
              <Image src={jeanGular} borderRadius='50%' maxWidth='80px' />
              <Text ml='12px'>
                <b>Jean Gular</b>
                <br />
                Développeur Angular
              </Text>
            </Flex>
            <Text>
              Un personnel aux petits soins, du café divin et des locaux vraiment accueillants. Je recommande vivement ce lieu, tant pour venir
              déguster un bon café que pour venir travailler.
            </Text>
          </Flex>
          <Flex flexDirection='column' flex='0 1 350px' mx='auto' mb='50px'>
            <Flex alignItems='center' mb='30px'>
              <Image src={emmaFemal} borderRadius='50%' maxWidth='80px' />
              <Text ml='12px'>
                <b>Emma Femal</b>
                <br />
                Experte du Digital
              </Text>
            </Flex>
            <Text>
              Exactement ce que je recherchais, un local pour travailler et me détendre en même temps ! A recommander sans modération. Ps: Le Neptune
              est délicieux !
            </Text>
          </Flex>
          <Flex flexDirection='column' flex='0 1 350px' mx='auto' mb='50px'>
            <Flex alignItems='center' mb='30px'>
              <Image src={marinDodousse} borderRadius='50%' maxWidth='80px' />
              <Text ml='12px'>
                <b>Marin Dodousse</b>
                <br />
                Entrepreneur du numérique
              </Text>
            </Flex>
            <Text>Je recommande vivement Gusto Coffee, c'est un endroit chaleureux, on s'y sent comme chez soi !</Text>
          </Flex>
        </Flex>
      </Flex>
    </Box>
  );
};

export default Home;
