import { Badge, Box, Flex, Image, Link, Text } from '@chakra-ui/core';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

const CoworkingPlace = ({ coworking }) => (
  <Box maxW='380px' p='20px'>
    <Link
      as={RouterLink}
      to={{
        pathname: `/coworking/${coworking.name.toLowerCase()}`,
        state: {
          coworking,
        },
      }}>
      <Image rounded='md' src={require(`../assets/images/salon-${coworking.name}.jpg`)} />
    </Link>
    <Flex align='baseline' mt={2}>
      <Badge variantColor='pink'>{coworking.location_category_id.name}</Badge>
      <Text ml={2} textTransform='uppercase' fontSize='sm' fontWeight='bold' color='pink.800' />
    </Flex>
    <Text mt={2} fontSize='xl' fontWeight='semibold' lineHeight='short'>
      {coworking.name}
    </Text>
    <Text mt={2}>
      {coworking.location_category_id.price}
      €/heure - Nombre de places : {coworking.location_category_id.place_number}
    </Text>
    {/* <Flex mt={2} align='center'>
      <Box color='orange.400' />
      <Text fontsize='sm' />
    </Flex> */}
  </Box>
);

export default CoworkingPlace;
