import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Link,
  useColorMode,
} from '@chakra-ui/core';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { useAuth } from '../Hooks/useAuth';

const LoginForm = () => {
  const { signin } = useAuth();
  const { colorMode } = useColorMode();

  const { handleSubmit, errors, register } = useForm();
  const [isSubmitting, setIsSubmitting] = useState(false);

  // message si une erreur survint lors de la connexion
  const [formMessage, setFormMessage] = useState({
    show: false,
    message: '',
  });
  const history = useHistory();

  const validateEmail = value => {
    let validationError;
    const emailPattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (!value) {
      validationError = 'Veuillez renseigner un email';
    } else if (!emailPattern.test(value)) {
      validationError = 'Veuillez entrer un email valide.';
    }
    return validationError || true;
  };

  const onSubmit = values => {
    setIsSubmitting(true);
    // On appelle le service pour créer un nouvel utilisateur
    signin(values.email, values.password).then(data => {
      setIsSubmitting(false);
      if (data === true) {
        setFormMessage({
          show: false,
          message: '',
        });
        history.push('/');
      } else {
        setFormMessage({
          show: true,
          message: data,
        });
      }
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <FormControl w='100%' isInvalid={errors.email}>
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input name='email' placeholder='jean-paul@gmail.com' ref={register({ validate: validateEmail })} backgroundColor='white' />
        <FormErrorMessage>{errors.email && errors.email.message}</FormErrorMessage>
      </FormControl>
      <FormControl w='100%' mt='10px' isInvalid={errors.password}>
        <FormLabel htmlFor='password'>Mot de passe</FormLabel>
        <Input name='password' placeholder='***********' ref={register({ required: true })} type='password' backgroundColor='white' />
        <FormErrorMessage>{errors.password && <span>Veuillez rensigner un mot de passe</span>}</FormErrorMessage>
      </FormControl>
      {requestSuccess()}
      <Flex alignItems='center' justifyContent='space-between' mt='30px' flexWrap='wrap'>
        <Button backgroundColor='marronClair' isLoading={isSubmitting} type='submit' color='white' _hover={{ backgroundColor: 'marronClair' }}>
          Se connecter
        </Button>
        <Flex flexDirection='column' alignItems='flex-end'>
          <Link as={RouterLink} to='/mot-de-passe-oublie' color={colorMode === 'light' ? 'black' : 'white'} mb='10px'>
            Mot de passe oublié ?
          </Link>
          <Link as={RouterLink} to='/signup' color={colorMode === 'light' ? 'black' : 'white'}>
            Pas encore inscrit ?
          </Link>
        </Flex>
      </Flex>
    </form>
  );

  function requestSuccess() {
    return formMessage.show ? (
      <Alert status='error' mt='10px' variant='subtle' flexDirection='column' justifyContent='center' textAlign='center' height='200px'>
        <AlertIcon size='40px' mr={0} />
        <AlertTitle mt={4} mb={1} fontSize='lg'>
          Erreur lors de la connexion
        </AlertTitle>
        <AlertDescription maxWidth='sm'>{formMessage.message}</AlertDescription>
      </Alert>
    ) : null;
  }
};

export default LoginForm;
