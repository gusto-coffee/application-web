import { LogoutOutlined, ShoppingCartOutlined, UserOutlined } from '@ant-design/icons';
import { Box, Button, Icon, Link, List, ListItem, useColorMode } from '@chakra-ui/core';
import React, { useEffect, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import bannerImage from '../assets/images/banner.png';
import { useAuth } from '../Hooks/useAuth';
import Banner from './Banner';
import DarkModeButton from './darkModeButton';

const Header = () => {
  const { user, signout } = useAuth();
  const { colorMode } = useColorMode();
  const [show, setShow] = useState(false);
  const handleToggle = () => setShow(!show);

  const [fixedMenu, setFixedMenu] = useState(false);

  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY >= 300) {
        setFixedMenu(true);
      } else {
        setFixedMenu(false);
      }
    });
  }, []);

  return (
    <>
      <Banner slogan='Gusto Coffee Coworking' subSlogan='La 3ème heure offerte de 9h à 16h' height='300' image={bannerImage} />
      <Box
        borderBottom='1px solid'
        borderColor='rgba(0, 0, 0, 0.08)'
        boxShadow='0 2px 2px -2px rgba(0,0,0,.2)'
        w='100%'
        position={fixedMenu && 'fixed'}
        top={fixedMenu && '0'}
        backgroundColor={colorMode === 'light' ? 'white' : 'marronClair'}
        zIndex={1}>
        <Box
          display={{ xs: 'flex', sm: 'flex', md: 'flex', lg: 'block' }}
          alignItems='flex-end'
          flexDirection={{ xs: 'column', sm: 'column', md: 'column' }}
          as='nav'
          px='20px'
          py='10px'
          maxWidth='1300px'
          margin='auto'
          position='relative'>
          <Button display={{ xs: 'block', sm: 'block', md: 'block', lg: 'none' }} onClick={handleToggle} mb={show && '30px'}>
            <Icon name='burgerMenu' size='35px' color={colorMode === 'light' ? 'black' : 'white'} />
          </Button>

          <List
            display={{ xs: show ? 'flex' : 'none', md: show ? 'flex' : 'none', lg: 'flex' }}
            width={{ sm: '100%' }}
            flexWrap={{ xs: 'wrap', sm: 'wrap' }}
            alignItems='center'
            justifyContent={{ sm: 'space-around', md: 'space-around', lg: 'space-between' }}>
            <ListItem py={{ xs: '15px', md: 2 }} px={{ xs: '20px', md: 6 }}>
              <Link as={RouterLink} to='/' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }} onClick={handleToggle}>
                Accueil
              </Link>
            </ListItem>
            <ListItem py={{ xs: '15px', md: 2 }} px={{ xs: '20px', md: 6 }}>
              <Link as={RouterLink} to='/coworking' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }} onClick={handleToggle}>
                Coworking
              </Link>
            </ListItem>
            <ListItem py={{ xs: '15px', md: 2 }} px={{ xs: '20px', md: 6 }}>
              <Link as={RouterLink} to='/la-carte' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }} onClick={handleToggle}>
                Produits
              </Link>
            </ListItem>
            <ListItem py={{ xs: '15px', md: 2 }} px={{ xs: '20px', md: 6 }}>
              <Link as={RouterLink} to='/services' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }} onClick={handleToggle}>
                Services
              </Link>
            </ListItem>
            <ListItem py={{ xs: '15px', md: 2 }} px={{ xs: '20px', md: 6 }}>
              <Link as={RouterLink} to='/a-propos' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }} onClick={handleToggle}>
                A Propos
              </Link>
            </ListItem>
            <ListItem py={{ xs: '15px', md: 2 }} px={{ xs: '20px', md: 6 }}>
              <Link as={RouterLink} to='/contact' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }} onClick={handleToggle}>
                Contact
              </Link>
            </ListItem>
            <ListItem>{isConnected()}</ListItem>
            <ListItem>
              <Link
                as={RouterLink}
                title='Mon panier'
                to='/panier'
                display={{ sm: show ? 'block' : 'none', md: 'block' }}
                mt={{ base: 4, md: 0 }}
                mr={5}
                cursor='pointer'
                _focus={{ boxShadow: 'none' }}>
                <ShoppingCartOutlined style={{ fontSize: '32px' }} />
              </Link>
            </ListItem>
            <ListItem>{logout()}</ListItem>

            <DarkModeButton />
          </List>
        </Box>
      </Box>
    </>
  );

  function logout() {
    return user ? (
      <LogoutOutlined
        display={{ sm: show ? 'block' : 'none', md: 'block' }}
        mt={{ base: 4, md: 0 }}
        mr={5}
        cursor='pointer'
        _focus={{ boxShadow: 'none' }}
        onClick={signout}
        title='Se déconnecter'
        style={{ fontSize: '30px', color: 'red' }}
      />
    ) : null;
  }
  function isConnected() {
    return user ? (
      <Link
        as={RouterLink}
        to='/profil'
        title='Voir mon profil'
        display={{ sm: show ? 'block' : 'none', md: 'block' }}
        mt={{ base: 4, md: 0 }}
        mr={5}
        _focus={{ boxShadow: 'none' }}>
        <UserOutlined style={{ fontSize: '30px' }} />
      </Link>
    ) : (
      <Link as={RouterLink} to='/login' _focus={{ boxShadow: 'none' }} _hover={{ textDecoration: 'none' }}>
        Se connecter / s'inscrire
      </Link>
    );
  }
};

export default Header;
