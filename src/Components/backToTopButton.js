import { Button, Icon } from '@chakra-ui/core';
import React from 'react';

const BackToTop = () => {
  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  return (
    <Button
      name='back_to_top'
      onClick={scrollTop}
      position='absolute'
      right='30px'
      bottom={['70px', '30px']}
      bg='transparent'
      _hover={{ backgroundColor: 'transparent', border: '1px solid white' }}>
      <Icon name='arrow-up' size='20px' color='white' _hover={{ color: 'black' }} />
    </Button>
  );
};

export default BackToTop;
