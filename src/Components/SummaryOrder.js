import { Box, Heading, Stack, Text } from '@chakra-ui/core';
import React from 'react';

const SummaryOrder = ({ state }) => {
  return (
    <>
      {showArticles()}

      {showReservations()}
    </>
  );

  function showArticles() {
    return state.articles.length > 0 ? (
      <Stack spacing={8} mb='50px'>
        <Text fontSize='26px' fontWeight='bold' mb='30px'>
          Produits
        </Text>
        {state.articles.map((article, key) => (
          <Box key={key.toString()} p={5} shadow='md' borderWidth='1px'>
            <Heading mb='5px' fontSize='xl'>
              ({article.quantity}) {article.product_id.name}
            </Heading>
            <Text mt={4}>{`Sous-total : ${article.price}€`}</Text>
          </Box>
        ))}
      </Stack>
    ) : null;
  }

  function showReservations() {
    return state.reservations.length > 0 ? (
      <Stack spacing={8} my='50px'>
        <Text fontSize='26px' fontWeight='bold' mb='30px'>
          Réservations
        </Text>
        {state.reservations.map((reservation, key) => (
          <Box key={key.toString()} p={5} shadow='md' borderWidth='1px'>
            <Heading mb='5px' fontSize='xl'>
              {reservation.reservation_id.location_id.name}
            </Heading>
            <Text mt={4}>
              Date : {String(reservation.reservation_id.day).padStart(2, '0')}/{String(reservation.reservation_id.month).padStart(2, '0')}/
              {reservation.reservation_id.year}
            </Text>
            <Text mt={4}>Heure de début : {reservation.reservation_id.start}h</Text>
            <Text mt={4}>Heure de fin : {reservation.reservation_id.end}h</Text>
          </Box>
        ))}
      </Stack>
    ) : null;
  }
};

export default SummaryOrder;
