import { Flex } from '@chakra-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { config } from '../config';
import Product from './Product';

const ProductList = () => {
  const [products, setProducts] = useState([]);
  const [dataloaded, setDataloaded] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(`${config.BASE_API_URL}/product`)
      .then(response => {
        if (response.status !== 200) {
          setError(response.data);
        } else {
          setProducts(response.data);
        }
        setDataloaded(true);
      })
      .catch(e => {
        setError(e);
        setDataloaded(true);
      });
  }, []);

  if (!dataloaded) return <div />;
  if (error) return <div>{error}</div>;
  return (
    <Flex justifyContent={{ sm: 'center', md: 'space-around', lg: 'space-around', xl: 'space-between' }} flexWrap='wrap'>
      {products
        .filter(product => product.category_id[0].name !== 'Services')
        .map(product => (
          <Product product={product} key={product._id} />
        ))}
    </Flex>
  );
};

export default ProductList;
