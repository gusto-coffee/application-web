import { Badge, Box, Button, Flex, Image, Text, useColorMode, useToast } from '@chakra-ui/core';
import React from 'react';
import useCart from '../Hooks/useCart';

const Product = ({ product }) => {
  const { addProductToCart } = useCart();
  const { colorMode } = useColorMode();

  const toast = useToast();
  return (
    <Flex flexDirection='column' flex='0 1 300px' mb='60px'>
      <Image src={require(`../assets/images/${product.imageName}`)} maxWidth='300px' h='200px' borderRadius='0.25rem' />
      <Box minH='200px'>
        <Box minHeight='180px'>
          <Text fontWeight='bold' fontSize='18px' my='10px'>
            {product.name}
          </Text>
          <Text>{product.description}</Text>
          <Text ml='auto' fontWeight='500'>
            {product.price} €
          </Text>
        </Box>
        {product.category_id.map(category => (
          <Badge w='max-content' px='8px' py='3px' backgroundColor='marron' color='white' key={category._id}>
            {category.name}
          </Badge>
        ))}
      </Box>
      <Button
        backgroundColor={colorMode === 'light' ? 'marronClair' : 'white'}
        color={colorMode === 'light' ? 'white' : 'marron'}
        mt='20px'
        onClick={() => {
          toast({
            title: 'Produit ajouté au panier',
            description: '',
            status: 'success',
            duration: 2000,
            isClosable: true,
          });
          addProductToCart(product);
        }}>
        Ajouter au panier
      </Button>
    </Flex>
  );
};

export default Product;
