import { Button, Icon, useColorMode } from '@chakra-ui/core';
import React from 'react';

const DarkModeButton = () => {
  const { colorMode, toggleColorMode } = useColorMode();

  return (
    <Button
      name='toggle_dark_mode'
      onClick={toggleColorMode}
      _hover={{ backgroundColor: 'transparent' }}
      _active={{ backgroundColor: 'transparent' }}
      bg='transparent'
      title={colorMode === 'light' ? 'Dark mode' : 'Light mode'}>
      {colorMode === 'light' ? (
        <Icon name='sun' size='24px' color={colorMode === 'light' ? 'black' : 'white'} />
      ) : (
        <Icon name='moon' size='24px' color={colorMode === 'light' ? 'black' : 'white'} />
      )}
    </Button>
  );
};

export default DarkModeButton;
