import { Flex } from '@chakra-ui/core';
import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { config } from '../config';
import CoworkingPlace from './CoworkingPlace';

const CoworkingList = () => {
  const [coworkings, setCoworkings] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios
      .get(`${config.BASE_API_URL}/coworking/location`)
      .then(response => {
        // console.log(response);
        if (response.status !== 200) {
          setError(response.data.error);
        } else {
          setCoworkings(response.data);
        }
      })
      .catch(e => setError(e));
  }, []);

  return error ? (
    { error }
  ) : (
    <Flex alignItems='center' justify='space-around' wrap='wrap'>
      {coworkings
        .filter(coworking => coworking.location_category_id.name !== 'Salle principale')
        .sort((a, b) => (a.location_category_id.price > b.location_category_id.price ? 1 : -1))
        .map((coworking, index) => (
          <CoworkingPlace coworking={coworking} key={index.toString()} />
        ))}
    </Flex>
  );
};

export default CoworkingList;
