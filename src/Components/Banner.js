import { Flex, Heading, Image } from '@chakra-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../assets/images/logo-header.png';

const Banner = ({ image, slogan, subSlogan, height }) => {
  return (
    <Flex
      background={`rgba(0, 0, 0) url(${image})`}
      h={`${height}px`}
      align='center'
      justify='center'
      flexDirection='column'
      bgPos='center'
      bgSize='cover'>
      <Link to='/'>
        <Image src={logo} alt='Gusto Coffee' width='50px' />
      </Link>
      <Heading as='h1' color='white' textAlign='center'>
        {slogan}
      </Heading>
      <Heading as='h2' color='white' textAlign='center'>
        {subSlogan}
      </Heading>
    </Flex>
  );
};

export default Banner;
