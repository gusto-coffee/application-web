import { Box, Button, Heading, Text } from '@chakra-ui/core';
import React from 'react';
import useCart from '../Hooks/useCart';

export default function Feature({ title, price, article, index, removeProductToCart }) {
  // const { removeProductToCart } = useCart();

  return (
    <Box p={5} shadow='md' borderWidth='1px'>
      <Heading mb='5px' fontSize='xl'>
        {title}
      </Heading>
      <Heading fontWeight='normal' fontSize='xs'>{`Quantité : ${article.quantity}`}</Heading>
      <Text mt={4}>{`${price}€`}</Text>
      <Button onClick={() => removeProductToCart(index)} display='block' marginLeft='auto' backgroundColor='marronClair' color='white' size='sm'>
        Supprimer
      </Button>
    </Box>
  );
}
