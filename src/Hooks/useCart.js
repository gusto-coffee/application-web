import axios from 'axios';
import { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { config } from '../config';
import { socket as io } from '../utils/socketIO';
import { useAuth } from './useAuth';

const today = new Date();
const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

const useCart = () => {
  const { user } = useAuth();
  const [cookie, setCookie] = useCookies(['shoppingBasket', 'reservation']);

  const [socket, setSocket] = useState(null);

  useEffect(() => {
    setSocket(io);
  }, []);

  const addProductToCart = (product, quantity = 1, checkbox = []) => {
    const basket = cookie.shoppingBasket || [];
    const options_checked = checkbox.filter(cb => cb.isChecked) || [];
    const priceOptions = 0.0;

    const productPrice = product.price;

    // ajout du prix total de l'article ((produit unitaire + options)*quantité)
    const totalPrice = (productPrice + priceOptions) * quantity;

    basket.push({
      _id: product._id,
      name: product.name,
      categories: product.category_id,
      productPrice,
      totalPrice,
      quantity,
      productPoints: product.points,
      points: product.points * quantity,
      options: options_checked,
      totalPriceOptions: priceOptions * quantity,
      priceOptions,
      promo: false,
    });
    setCookie('shoppingBasket', basket, {
      expires: tomorrow,
    });
  };

  const confirmCart = () => {
    const articles = [];
    const reservationsOrders = [];
    let totalPrice = 0.0;
    let fidelityPoints = 0;

    if (cookie.shoppingBasket)
      cookie.shoppingBasket.forEach(item => {
        totalPrice += item.totalPrice;
        fidelityPoints += item.points;
        const option_ids = [];
        // ajout des options
        item.options.forEach(option => {
          option_ids.push(option._id);
        });
        // mise en forme de la requête des articles
        articles.push({
          product_id: item._id,
          option_ids,
          quantity: item.quantity,
          price: item.totalPrice,
        });
      });

    if (cookie.reservation)
      cookie.reservation.forEach(reservation => {
        totalPrice += reservation.totalPrice;
        fidelityPoints += reservation.fidelity_points;
        reservationsOrders.push({ reservation_id: reservation.reservation_id, price: reservation.totalPrice });
      });

    return axios
      .post(`${config.BASE_API_URL}/shopping-basket/confirm`, {
        articles,
        reservations: reservationsOrders,
        total_price: totalPrice,
        preview_fidelity_points: fidelityPoints,
      })
      .then(({ data, status }) => {
        if (status === 201) {
          localStorage.setItem('basket', JSON.stringify(data._id));
          localStorage.setItem('confirmBasket', JSON.stringify(2));
          return true;
        }
      });
  };

  const getBasket = id => {
    return axios.get(`${config.BASE_API_URL}/shopping-basket/${id}`).then(({ data, status }) => {
      if (status === 200) return data;
    });
  };

  const confirmPayment = id => {
    return axios
      .post(`${config.BASE_API_URL}/order/payment`, {
        _id: id,
        currency: 'euro',
        payment_method: 'bank card',
      })
      .then(({ data, status }) => {
        if (status === 201) return data._id;
      });
  };

  const removeProductToCart = index => {
    const newCart = cookie.shoppingBasket || [];
    newCart.splice(index, 1);
    setCookie('shoppingBasket', newCart, {
      expires: tomorrow,
    });
  };

  const calculPrice = (price, heureDebut, heureFin) => {
    // On calcul le prix fois le nbr d'heures
    let total = price * (heureFin - heureDebut);
    // Si la reservation est entre 9h et 16h (heures creuses)
    if (heureDebut >= 9 && heureFin <= 16) {
      // Si la reservation est sur 3h d'affilées
      if (heureFin - heureDebut === 3) {
        // On soustrait une heure au total
        total -= price;
      }
    }
    return total;
  };

  const calculPoints = (points, heureDebut, heureFin) => {
    return points * (heureFin - heureDebut);
  };

  const addReservationToCart = data => {
    socket.emit(
      'new-hour-reservation',
      {
        user_id: data.user_id,
        location_id: data.location_id,
        year: data.year,
        month: data.month,
        day: data.day,
        start: data.start,
        end: data.end,
        price: calculPrice(data.prix, data.start, data.end),
        fidelity_points: calculPoints(data.points, data.start, data.end),
      },
      response => {
        if (response.code === 201) {
          let newReservations = cookie.reservation || [];
          newReservations = newReservations.concat({
            reservation_id: {
              _id: response.data._id,
              user_id: user ? user.userId : null,
              location_id: data.location_id,
              year: data.year,
              month: data.month,
              day: data.day,
              start: data.start,
              end: data.end,
              price: data.prix,
            },
            location_id: { name: data.name, location_category_id: data.location_category_id },
            fidelity_points: calculPoints(data.points, data.start, data.end),
            totalPrice: calculPrice(data.prix, data.start, data.end),
          });
          setCookie('reservation', newReservations, {
            expires: tomorrow,
          });
        }
      },
    );
  };

  const removeReservationFromCart = (data, index) => {
    // supprimer une réservation du panier
    socket.emit('remove-hour-reservation', { _id: data.reservation_id._id }, ({ code }) => {
      if (code === 200) {
        const newReservations = cookie.reservation || [];
        newReservations.splice(index, 1);
        setCookie('reservation', newReservations, {
          expires: tomorrow,
        });
      }
    });
  };

  const getOrder = id => {
    return axios.get(`${config.BASE_API_URL}/order/${id}`).then(({ status, data }) => {
      if (status === 200) return data;
    });
  };

  const getOrders = () => {
    return axios
      .get(`${config.BASE_API_URL}/order`)
      .then(({ status, data }) => {
        if (status === 200) return data;
      })
      .catch(() => {
        return [];
      });
  };

  return {
    addProductToCart,
    addReservationToCart,
    removeReservationFromCart,
    removeProductToCart,
    confirmCart,
    getBasket,
    getOrder,
    getOrders,
    confirmPayment,
  };
};

export default useCart;
