# Gusto Coffee web app ☕️

Ce projet a été généré avec [Create React App](https://github.com/facebook/create-react-app) ⚛️

## Prérequis 🔧

- [Node 10.x.x](https://nodejs.org/en/)
- NPM (S'installe automatiquement lorsque l'on installe Node)

## Installation 🔄

```
git clone
```

```
cd <project>
```

```
npm install
```

## Lancement 🚀

Pour lancer le serveur de developpement, exécuter la commande suivante :

```
npm start
```

## Utilisation 🔥

- Ouvir votre navigateur sur l'url [http://localhost:3000](http://localhost:3000) afin de voir l'application.
